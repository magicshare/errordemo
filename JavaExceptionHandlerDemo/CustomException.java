/**
 * @(#)DataException.java, 2014-9-28.
 */
/**
 * 
 * @author hzcaoyanming
 *
 */
public class CustomException extends Exception{
	
	/** NO_ERROR 8*/
	public static final int NO_ERROR =0;
	
	/** UNKNOW_ERROR */
	public static final int UNKNOWN_ERROR = 1;

    /** IO_ERROR */
    public static final int NET_IO_ERROR = 2;
    
    /** INVALID_LITERAL_LENGTH */
    public static final int INVALID_LITERAL_LENGTH = 3;

    /** SERVER_CLOSE_CONNECTION */
    public static final int SERVER_CLOSE_CONNECTION = 4;
	
    /** JSON_TRANFORM_FAILED */
    public static final int JSON_TRANFORM_FAILED = 5;
    
    public static final int TRANSITION_EXCEPTION = 6;
    
    protected int errorType = 0;
    
    private Object errorData;
    
    /**
     * 
     * @param errorType   the type of exception
     * @param errorMsg    the msg of exception
     * @param errorData   the extra data of exception
     */
    public CustomException (int errorType, String errorMsg, Object errorData){
    	super(errorMsg);
    	this.errorType = errorType;
    	this.errorData = errorData;
    }
    
    
    /**
     * @param type		 the type of exception.
     */
    public CustomException(int type) {
        this(type, "", null);
    }
    
    /**
     * @param type       the type of exception.
     * @param message    the message of exception.
     * @param throwable  the throwable object.
     */
    public CustomException(int type, String message, Throwable throwable) {
        super(message, throwable);
        errorType = type;
        errorData = null;
    }
    
    /**
     * @param message  the message of exception.
     * @param throwable    the throwable object.
     */
    public CustomException(String message, Throwable throwable) {
        this(UNKNOWN_ERROR, message, throwable);
    }
    
    
    /**
     * 
     * @return errorData
     */
    public Object getErrorData() {
        return errorData;
    }

    /**
     * 
     * @return errorType
     */
    public int getErrorType() {
        return errorType;
    }
}
