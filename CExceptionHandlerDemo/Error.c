/*
 * Error.c
 *
 *  Created on: 2014-9-28
 *      Author: hzcaoyanming
 *      Reference: Chen Hao-编程修养-11
 */


#include <stdlib.h>
#include <stdio.h>

#define ERR_NO_ERROR 0               	/* NO ERROR */
#define UNKNOWN_ERROR 1				 	/* UNKNOWN ERROR */
#define MEMEROY_IS_NOT_ENOUGH 2       	/* MEMEROY IS NOT ENOUGH */
#define PERMISSION_DENIED 3			 	/* PERMISSION DENIED */
#define BAD_CONFIGURATION_FILE_FORMAT 4	/* BAD CONFIGURATION FILE FORMAT */
#define TIME_OUT 5					 	/* TIME OUT */


/* 声明错误的全局变量 */
long errorno = 0;

const char* errmsg[] = {
		"NO ERROR",
		"UNKNOWN ERROR",
		"MEMEROY IS NOT ENOUGH",
		"PERMISSION DENIED",
		"BAD CONFIGURATION FILE FORMAT",
		"TIME OUT"
};



/* 打印错误信息函数 */
void perror(char* info)
{
	if( info )
	{
		printf("%s: %s\n", info, errmsg[errorno]);
		return;
	}

	printf("Error: %s\n", errmsg[errorno]);
}




int main(int argc, char **argv)
{
	char *p =  (char *)malloc(sizeof(char *));
	if(!p)
	{
		errorno = 2;
		perror("main()");
	}
	free(p);
	return 0;
}
